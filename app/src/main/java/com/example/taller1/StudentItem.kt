package com.example.taller1

import com.example.taller1.databinding.ItemStudentBinding
import com.xwray.groupie.databinding.BindableItem

class StudentItem (val name: String, val code: String): BindableItem<ItemStudentBinding>() {
    override fun bind(viewBinding: ItemStudentBinding, position: Int) {
        viewBinding.nameTextView.text = name
        viewBinding.codeTextView.text = code
    }
    override fun getLayout(): Int {
        return R.layout.item_student
    }
}