package com.example.taller1

import com.example.taller1.databinding.StudentGroupieBinding
import com.xwray.groupie.ExpandableGroup
import com.xwray.groupie.ExpandableItem
import com.xwray.groupie.databinding.BindableItem

class StudentGroupie: BindableItem<StudentGroupieBinding>(), ExpandableItem {
    lateinit var expandibleGroupie: ExpandableGroup

    override fun bind(viewBinding: StudentGroupieBinding, position: Int) {
        viewBinding.buttonList.setOnClickListener{
            expandibleGroupie.onToggleExpanded()
        }
    }
    override fun getLayout(): Int {
        return R.layout.student_groupie
    }
    override fun setExpandableGroup(onToggleListener: ExpandableGroup) {
        this.expandibleGroupie= onToggleListener
    }

}