package com.example.taller1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.taller1.databinding.ActivityMainBinding
import com.xwray.groupie.ExpandableGroup
import com.xwray.groupie.GroupieAdapter

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //generar binding
        val binding = DataBindingUtil.setContentView<ActivityMainBinding>(this, R.layout.activity_main)

        //manipular binding
        binding.studentsRecyclerView.layoutManager = LinearLayoutManager(this)

        val list = ExpandableGroup(StudentGroupie())
        list.add(StudentItem("Juan Camilo Bautista Roa","50119"))
        list.add(StudentItem("Deisy Tatiana Gómez Fernández","74233"))
        list.add(StudentItem("Jose Andrés Perea Camelo","34922"))

        val adapter = GroupieAdapter()
        binding.studentsRecyclerView.adapter = adapter
        adapter.add(list)

    }
}